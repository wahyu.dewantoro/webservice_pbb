/*
SQLyog Ultimate v9.02 
MySQL - 5.5.5-10.1.28-MariaDB : Database - beagle
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Data for the table `ms_assign_role` */

insert  into `ms_assign_role`(`id_inc`,`ms_pengguna_id`,`ms_role_id`) values (1,1,1);

/*Data for the table `ms_menu` */

insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (1,'Sistem','#',0,99,'mdi mdi-settings-square');
insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (2,'role','role',1,1,'fa fa-angle-right');
insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (3,'menu','menu',1,2,'fa fa-angle-right');
insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (6,'Pengguna','Pengguna',1,3,'fa fa-users');
insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (7,'Asign Role','AsignRole',1,4,'fa fa-lock');
insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (8,'Dashboard','Welcome',0,0,'mdi mdi-home');
insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values (11,'Test','Test',0,1,'mdi mdi-file-text');

/*Data for the table `ms_pengguna` */

insert  into `ms_pengguna`(`id_inc`,`nama`,`username`,`password`) values (1,'Wahyu Dewantoro','admin','');
insert  into `ms_pengguna`(`id_inc`,`nama`,`username`,`password`) values (2,'Pengguna A','user1','');

/*Data for the table `ms_privilege` */

insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (1,1,1,1,NULL,NULL,NULL);
insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (2,1,2,1,1,1,1);
insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (3,1,3,1,1,1,1);
insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (8,1,6,1,1,1,NULL);
insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (9,1,7,1,1,1,1);
insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (10,1,8,1,1,1,NULL);
insert  into `ms_privilege`(`id_inc`,`ms_role_id`,`ms_menu_id`,`status`,`create`,`update`,`delete`) values (13,1,11,1,1,1,1);

/*Data for the table `ms_role` */

insert  into `ms_role`(`id_inc`,`nama_role`) values (1,'Sys Admin');

/*Data for the table `test` */

insert  into `test`(`id`,`nama`) values (2,'Testing asd');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

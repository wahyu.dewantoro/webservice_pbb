<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Session Helper
 *
 * A simple session class helper for Codeigniter
 *
 * @package     Codeigniter Session Helper
 * @author      Dwayne Charrington
 * @copyright   Copyright (c) 2014, Dwayne Charrington
 * @license     http://www.apache.org/licenses/LICENSE-2.0.html
 * @link        http://ilikekillnerds.com
 * @since       Version 1.0
 * @filesource
 */
if (!function_exists('showMenu'))
{
    function showMenu($kode_group)
    {
        $CI = get_instance();
        $CI->load->database();
        $qmenu0  =$CI->db->query("SELECT distinct a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND parent='0'
                                          ORDER BY sort ASC")->result_array();

        $varmenu='';
              foreach ($qmenu0 as $row0) {
                $parent  =$row0['ID_INC'];
                $qmenu1  =$CI->db->query("SELECT distinct a.*
                                          FROM ms_menu a
                                          JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                          WHERE ms_role_id in ($kode_group)
                                          AND STATUS='1'
                                          AND parent='$parent'
                                          ORDER BY sort ASC");
                $cekmenu =$qmenu1->num_rows();
                $dmenu1  =$qmenu1->result_array();
                  if($cekmenu>0){
                    $varmenu.= "<li class='parent'>
                      <a href='#' > <i class='".$row0['ICON']."'></i>  <span>".ucwords($row0['NAMA_MENU'])."</span> <span class='menu-arrow'></span></a>";
                        $varmenu.= "<ul class='sub-menu'>";
                          foreach($dmenu1 as $row1){
                              $varmenu.= "<li>".anchor(strtolower($row1['LINK_MENU']),"<i class='mdi mdi-chevron-right'></i> <span>".ucwords($row1['NAMA_MENU']."</span>"),'class="nav-link "')."</li>";
                          }
                      $varmenu.= "</ul>
                      </li>";
                      }else{
                       $varmenu.= "<li>".anchor(strtolower($row0['LINK_MENU']),"<i class='".$row0['ICON']."'></i> <span>".ucwords($row0['NAMA_MENU'].'</span>'),'class="nav-link "')."</li>";
                    }
                  }
           echo $varmenu;
    }
}


if (!function_exists('errorbos'))
{
    function errorbos()
    {
            $CI = &get_instance();
            $err= $CI->load->view('errors/403','',true);
            echo $err;
            die();

    }
}

if (!function_exists('cek'))
{
    function cek($pengguna,$url,$var)
    {
        $CI = get_instance();
        $CI->load->database();

        if(empty($var)){
            errorbos();
        }
        switch ($var) {
            case 'read':
                # code...
                $vv='status';
                break;
            default:
                # code...
                $vv=$var;
                break;
        }

        $res=$CI->db->query("SELECT a.*
                                FROM ms_privilege a
                                JOIN ms_menu b ON a.ms_menu_id=b.id_inc
                                JOIN ms_assign_role d on d.ms_role_id=a.ms_role_id
                                JOIN ms_pengguna c ON c.id_inc=d.ms_pengguna_id
                                WHERE c.id_inc=$pengguna AND upper(REPLACE(link_menu,'/','.'))=upper('$url') AND a.".$vv." ='1'")->row();

        $ff=$CI->db->query("SELECT count(a.id_inc) cek
                                FROM ms_privilege a
                                JOIN ms_menu b ON a.ms_menu_id=b.id_inc
                                JOIN ms_assign_role d on d.ms_role_id=a.ms_role_id
                                JOIN ms_pengguna c ON c.id_inc=d.ms_pengguna_id
                                WHERE c.id_inc=$pengguna AND upper(REPLACE(link_menu,'/','.'))=upper('$url') AND a.".$vv." ='1'")->row();

          // echo $CI->db->last_query();
          // die();
        if($ff->CEK > 0){
                return array(
                    'read'=>$res->STATUS,
                    'create'=>$res->CREATED,
                    'update'=>$res->UPDATED,
                    'delete'=>$res->DELETED
                );

        }else{
            errorbos();

        }
    }
}






if (!function_exists('angka'))
{
    function angka($angka)
    {
      return number_format($angka,'0','','.');
    }
}

/*if (!function_exists('formatnop'))
{
    function formatnop($angkab)
    {
      // return $angkab;
      $angkaa=$angkab;

      $c = "";
       $panjang =strlen($angkaa);

       if ($panjang <=2){
        $c=$angkaa;
       }else if ($panjang >2 && $panjang <=4) {
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2);

       }else if($panjang>4 && $panjang <=7){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3);
       }else if($panjang>7 && $panjang <= 10){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3);
       }else if($panjang>1 && $panjang<=14 ){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3).'.'.substr($angkaa , 11, 3);

       }else if($panjang> 14 && $panjang<=17){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3).'.'.substr($angkaa , 11, 3).'.'.substr($angkaa , 13, 4);
       }else{
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3).'.'.substr($angkaa , 11, 3).'.'.substr($angkaa , 13, 4).'.'.substr($angkaa , 17, 1);
       }


       return $c;

    }
}*/

if (!function_exists('formatnop'))
{
    function formatnop($angkab)
    {
      // return $angkab;
      $angkaa=$angkab;

      $c = "";
       $panjang =strlen($angkaa);

       if ($panjang <=2){
        $c=$angkaa;
       }else if ($panjang >2 && $panjang <=4) {
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2);

       }else if($panjang>4 && $panjang <=7){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3);
       }else if($panjang>7 && $panjang <= 10){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3);
       }else if($panjang>1 && $panjang<=14 ){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3).'.'.substr($angkaa , 10, 3);

       }else if($panjang> 14 && $panjang<=17){
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3).'.'.substr($angkaa , 10, 3).'.'.substr($angkaa , 13, 4);
       }else{
        $c=substr($angkaa ,  0, 2).'.'.substr($angkaa , 2, 2).'.'.substr($angkaa , 4, 3).'.'.substr($angkaa , 7,3).'.'.substr($angkaa , 10, 3).'.'.substr($angkaa , 13, 4).'.'.substr($angkaa , 17, 1);
       }


       return $c;

    }
}
if( ! function_exists('KdPengesahan'))
{
	function KdPengesahan($digits = 16){
	    $i = 0; //counter
	    $pin = ""; //our default pin is blank.
	    while($i < $digits){
	        //generate a random number between 0 and 9.
	        $pin .= mt_rand(0, 9);
	        $i++;
	    }
	    return '3504'.$pin;
	}
}


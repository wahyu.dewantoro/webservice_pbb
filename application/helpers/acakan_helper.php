<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('acak'))
{
	function acak($str) {
		    $kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		    $hasil='';
		    for ($i = 0; $i < strlen($str); $i++) {
		        $karakter = substr($str, $i, 1);
		        $kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		        $karakter = chr(ord($karakter)+ord($kuncikarakter));
		        $hasil .= $karakter;
		        
		    }
		    return urlencode(base64_encode($hasil));
		}

}

if( ! function_exists('rapikan'))
{
	function rapikan($str) {
		    $str = base64_decode(urldecode($str));
		    $hasil = '';
		    $kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		    for ($i = 0; $i < strlen($str); $i++) {
		        $karakter = substr($str, $i, 1);
		        $kuncikarakter = substr($kunci, ($i % strlen($kunci))-1, 1);
		        $karakter = chr(ord($karakter)-ord($kuncikarakter));
		        $hasil .= $karakter;
		        
		    }
		    return $hasil;
		}
}

if( ! function_exists('KdPengesahan'))
{
	function KdPengesahan($digits = 12){
	    $i = 0; //counter
	    $pin = ""; //our default pin is blank.
	    while($i < $digits){
	        //generate a random number between 0 and 9.
	        $pin .= mt_rand(0, 9);
	        $i++;
	    }
	    return '3507'.$pin;
	}
}

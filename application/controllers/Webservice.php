<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
class Webservice extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        if (ENVIRONMENT == 'development') {
            $pass = '1a1dc91c907325c69271ddf0c944bc72';
        } else {
            $pass = '1a1dc91c907325c69271ddf0c944bc72';
        }


        if (!(isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])
            && $_SERVER['PHP_AUTH_USER'] == 'pbb'
            && md5($_SERVER['PHP_AUTH_PW']) == $pass)) {
            header('WWW-Authenticate: Basic realm="Restricted area"');
            header('HTTP/1.1 401 Unauthorized');
            exit;
        }
    }

    private function ceksppt($nop, $tahun_pajak)
    {
        // $res = $this->db->query("SELECT a.*,get_denda@to17(a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op,thn_pajak_sppt,pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt,sysdate) denda from SPPT_OLTP@TOSPO14 a where  thn_pajak_sppt='$tahun_pajak'and kd_propinsi =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1) ")->row();
        $res = $this->db->query("SELECT a.*,get_denda@to17(a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op,thn_pajak_sppt,pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt,sysdate) denda from SPPT_OLTP a 
        where  thn_pajak_sppt='$tahun_pajak'and kd_propinsi =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1) ")->row();
        return $res;
    }

    private function dataNop($nop)
    {
        return $this->db->query("SELECT * FROM DAT_OBJEK_PAJAK@TO17
                                where kd_propinsi =substr('$nop',1,2) 
                                 and kd_dati2=substr('$nop',3,2)
                                 and kd_kecamatan=substr('$nop',5,3)
                                 and kd_kelurahan=substr('$nop',8,3)
                                and kd_blok=substr('$nop',11,3) 
                                and no_urut=substr('$nop',14,4) 
                                and kd_jns_op=substr('$nop',18,1)")->row();
    }

    private function cekBayar($nop, $tahun)
    {
        return $this->db->query("SELECT * FROM PEMBAYARAN_SPPT@to17
                                    WHERE THN_PAJAK_SPPT='$tahun'
                                    AND KD_PROPINSI =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1)")->row();
    }

    private function getKecamatan($nop)
    {
        return $this->db->query("SELECT * FROM REF_KECAMATAN@TO17
                                    where kd_kecamatan =substr('$nop',5,3)")->row();
    }

    private function getKelurahan($nop)
    {
        return $this->db->query("SELECT * FROM REF_KELURAHAN@TO17
                               where kd_kecamatan=substr('$nop',5,3)
                                 and kd_kelurahan=substr('$nop',8,3)")->row();
    }

    public function inquiry()
    {
        // http_response_code(200);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $nop         = $this->input->get('Nop');
            $tahun_pajak = $this->input->get('TahunPajak');
            $pokok       = $this->input->get('Pokok');
            $merchant    = $this->input->get('kode_bank');

            $des = '';
            if (empty($nop)) {
                $des .= 'parameter nop harus di sertakan,';
            }

            if (strlen($nop) != 18) {
                $des .= 'panjang nop harus 18 digit,';
            }

            if (empty($merchant)) {
                $des .= 'parameter kode bank harus di sertakan,';
            }

            if (empty($tahun_pajak)) {
                $des .= 'parameter tahun pajak harus disertakan';
            }

            if ($des == '') {
                // $cb = $this->db->query("select * 
                //                     from ref_bank@TOLAYANAN where kode_bank=$merchant")->row();
                $cb = $this->db->query("SELECT * FROM LAYANAN.REF_BANK where kode_bank=$merchant and status=1")->row();
                if (empty($cb)) {
                    $data = array();
                    $data = array(

                        "Status"     =>  array(
                            "IsError"      => "True",
                            "ResponseCode" => '91',
                            "ErrorDesc"    => "Kode bank tidak terdaftar"
                        )
                    );
                    header('Content-Type: application/json');
                    // echo json_encode($data);
                    $konten = json_encode($data);
                    $this->db->set('IP_ADDRESS', $this->input->ip_address());
                    $this->db->set('KONTEN', $konten);
                    $this->db->set('KODE_BANK', $merchant);
                    $this->db->set('TANGGAL', 'sysdate', false);
                    $this->db->insert('LOG_SERVICE');
                    echo $konten;
                    die();
                }


                $query = $this->ceksppt($nop, $tahun_pajak);
                $data = array();
                if (!empty($query->THN_PAJAK_SPPT)) {
                    $cb = $this->cekBayar($nop, $tahun_pajak);
                    if (count($cb) > 0) {
                        $data = array(
                            "Status"        => array(
                                "IsError" => "True",
                                "ResponseCode" => '13',
                                "ErrorDesc" => "Data tagihan telah lunas"
                            )
                        );
                        header('Content-Type: application/json');
                        // echo json_encode($data, 200);
                        $konten = json_encode($data);
                        $this->db->set('IP_ADDRESS', $this->input->ip_address());
                        $this->db->set('KONTEN', $konten);
                        $this->db->set('KODE_BANK', $merchant);
                        $this->db->set('TANGGAL', 'sysdate', false);
                        $this->db->insert('LOG_SERVICE');
                        echo $konten;
                    } else {

                        $dtnop = $this->dataNop($nop);
                        $kec   = $this->getKecamatan($nop);
                        $kel   = $this->getKelurahan($nop);

                        $data = array(
                            "Nop"            => $query->KD_PROPINSI . $query->KD_DATI2 . $query->KD_KECAMATAN . $query->KD_KELURAHAN . $query->KD_BLOK . $query->NO_URUT . $query->KD_JNS_OP,
                            "Nama"           =>  $query->NM_WP_SPPT,
                            "Alamatobjek"    =>  $dtnop->JALAN_OP . ' ' . $dtnop->BLOK_KAV_NO_OP . ' RT ' . $dtnop->RT_OP . ' / RW ' . $dtnop->RW_OP,
                            "Kelurahanobjek" => $kel->NM_KELURAHAN,
                            "Kecamatanobjek" => $kec->NM_KECAMATAN,
                            "Kabupaten"      => "MALANG",
                            "TahunPajak"     => $query->THN_PAJAK_SPPT,
                            "JatuhTempo"     => $query->TGL_JATUH_TEMPO_SPPT,
                            "Pokok"          =>  (int) $query->PBB_YG_HARUS_DIBAYAR_SPPT,
                            "Denda"          => (int) $query->DENDA,
                            "Total"          => (int) $query->PBB_YG_HARUS_DIBAYAR_SPPT + (int) $query->DENDA,
                            "Status"         => array(
                                "IsError" => "False",
                                "ResponseCode" => '00',
                                "ErrorDesc" => "Inquiry sukses"
                            )
                        );
                        header('Content-Type: application/json');
                        // echo json_encode($data, 200);
                        $konten = json_encode($data, 200);
                        $this->db->set('IP_ADDRESS', $this->input->ip_address());
                        $this->db->set('KONTEN', $konten);
                        $this->db->set('KODE_BANK', $merchant);
                        $this->db->set('TANGGAL', 'sysdate', false);
                        $this->db->insert('LOG_SERVICE');
                        echo $konten;
                    }
                } else {
                    $data = array(
                        // "Nop"   =>  $KODE_BILING,
                        "Status"        =>  array(
                            "IsError" => "True",
                            "ResponseCode" => '10',
                            "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                        )
                    );

                    header('Content-Type: application/json');
                    // echo json_encode($data);
                    $konten = json_encode($data);
                    $this->db->set('IP_ADDRESS', $this->input->ip_address());
                    $this->db->set('KONTEN', $konten);
                    $this->db->set('KODE_BANK', $merchant);
                    $this->db->set('TANGGAL', 'sysdate', false);
                    $this->db->insert('LOG_SERVICE');
                    echo $konten;
                }
            } else {
                $data = array(

                    "Status" =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '99',
                        "ErrorDesc" => "System Error. " . substr($des, 0, -1)
                    )
                );
                header('Content-Type: application/json');
                // echo json_encode($data);
                $konten = json_encode($data);
                $this->db->set('IP_ADDRESS', $this->input->ip_address());
                $this->db->set('KONTEN', $konten);
                $this->db->set('KODE_BANK', $merchant);
                $this->db->set('TANGGAL', 'sysdate', false);
                $this->db->insert('LOG_SERVICE');
                echo $konten;
            }
        }
    }  //end inquiry




    public function payment()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $content  = file_get_contents("php://input");
            $decoded  = json_decode($content, true);


            $des = '';
            if (empty($decoded['Nop'])) {
                $des .= 'parameter nop harus di sertakan,';
            } else {
                if (strlen($decoded['Nop']) != 18) {
                    $des .= 'panjang nop harus 18 digit,';
                }
            }

            if (empty($decoded['kode_bank'])) {
                $des .= 'parameter kode bank harus di sertakan,';
            }

            if (empty($decoded['DateTime'])) {
                $des .= 'parameter date time harus di sertakan,';
            }

            if (empty($decoded['Tahun'])) {
                $des .= 'parameter tahun pajak harus di sertakan,';
            }

            if (empty($decoded['Pokok'])) {

                $des .= 'parameter pokok harus di sertakan,';
            }

            if (empty($decoded['Total'])) {
                $des .= 'parameter total harus di sertakan,';
            }





            if ($des != '') {

                $data = array(

                    "Status" =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '99',
                        "ErrorDesc" => "System Error. " . substr($des, 0, -1)
                    )
                );
                header('Content-Type: application/json');
                // echo json_encode($data);
                $konten = json_encode($data);
                $this->db->set('IP_ADDRESS', $this->input->ip_address());
                $this->db->set('KONTEN', $konten);
                $this->db->set('KODE_BANK', $merchant);
                $this->db->set('TANGGAL', 'sysdate', false);
                $this->db->insert('LOG_SERVICE');
                echo $konten;
                die();
            }

            $nop      = $decoded['Nop'];
            $merchant = $decoded['kode_bank'];
            $datetime = $decoded['DateTime'];
            $tahun    = $decoded['Tahun'];
            $pokok    = $decoded['Pokok'];
            $denda    = $decoded['Denda'];
            $total    = $decoded['Total'];



            // $cb = $this->db->query("select * from ref_bank@TOLAYANAN where kode_bank=$merchant")->row();
            $cb = $this->db->query("SELECT * FROM LAYANAN.REF_BANK where kode_bank=$merchant and status=1")->row();
            if (empty($cb)) {
                $data = array();
                $data = array(
                    "Status"     =>  array(
                        "IsError"      => "True",
                        "ResponseCode" => '91',
                        "ErrorDesc"    => "Kode bank tidak terdaftar"
                    )
                );
                header('Content-Type: application/json');
                // echo json_encode($data);
                $konten = json_encode($data);
                $this->db->set('IP_ADDRESS', $this->input->ip_address());
                $this->db->set('KONTEN', $konten);
                $this->db->set('KODE_BANK', $merchant);
                $this->db->set('TANGGAL', 'sysdate', false);
                $this->db->insert('LOG_SERVICE');
                echo $konten;
                die();
            } //end bank    

            $q = $this->ceksppt($nop, $tahun);
            if (!empty($q->KD_PROPINSI)) {
                $cb = $this->cekBayar($nop, $tahun);
                if ($cb > 0) {
                    // tagihan telah lunas
                    $data = array();
                    $data = array(
                        "Status"        =>  array(
                            "IsError" => "True",
                            "ResponseCode" => '13',
                            "ErrorDesc" => "Data Tagihan Telah Lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    // echo json_encode($data);
                    $konten = json_encode($data);
                    $this->db->set('IP_ADDRESS', $this->input->ip_address());
                    $this->db->set('KONTEN', $konten);
                    $this->db->set('KODE_BANK', $merchant);
                    $this->db->set('TANGGAL', 'sysdate', false);
                    $this->db->insert('LOG_SERVICE');
                    echo $konten;
                } else {
                    // belum lunas
                    if (((int) $q->PBB_YG_HARUS_DIBAYAR_SPPT + (int) $q->DENDA) != $total) {
                        $data = array();
                        $data = array(
                            "Status"     =>  array(
                                "IsError"      => "True",
                                "ResponseCode" => '14',
                                "ErrorDesc"    => "Jumlah tagihan yang dibayarkan tidak sesuai"
                            )
                        );
                        header('Content-Type: application/json');
                        // echo json_encode($data);
                        $konten = json_encode($data);
                        $this->db->set('IP_ADDRESS', $this->input->ip_address());
                        $this->db->set('KONTEN', $konten);
                        $this->db->set('KODE_BANK', $merchant);
                        $this->db->set('TANGGAL', 'sysdate', false);
                        $this->db->insert('LOG_SERVICE');
                        echo $konten;
                    } else {
                        // sukses
                        $this->db->trans_start();
                        $kdp = KdPengesahan();
                        $jml_sppt_yg_dibayar = $denda + $pokok;

                        $aa = "";
                        $a = $this->db->query("INSERT INTO PEMBAYARAN_SPPT(KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE, KD_KANWIL_BANK, KD_KPPBB_BANK, KD_BANK_TUNGGAL, KD_BANK_PERSEPSI, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT,  PENGESAHAN,KODE_BANK_BAYAR)
                                                    VALUES ( substr('" . $nop . "',1,2),substr('" . $nop . "',3,2),substr('" . $nop . "',5,3),substr('" . $nop . "',8,3),substr('" . $nop . "',11,3),substr('" . $nop . "',14,4),substr('" . $nop . "',18,1), '" . $tahun . "', '1', '00', '00', '00', '00', '04', '" . $denda . "', '" . $jml_sppt_yg_dibayar . "', to_date('$datetime','yyyy-mm-dd hh24:mi:ss'), sysdate, '090909091', '$kdp','$merchant')");
                        if ($a) {
                            $aa .= "1. sukses<br>";
                        } else {
                            $aa .= "[" . $this->db->last_query() . " ] <br>";
                        }

                        $b = $this->db->query("UPDATE  sppt@to17 set status_pembayaran_sppt=1 where thn_pajak_sppt='$tahun'and kd_propinsi =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1)");
                        if ($b) {
                            $aa .= "2. sukses<br>";
                        } else {
                            $aa .= "2. gagal <br>";
                        }

                        $c = $this->db->query("INSERT INTO PEMBAYARAN_SPPT@to17 (KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, THN_PAJAK_SPPT, PEMBAYARAN_SPPT_KE,KD_KANWIL,KD_KANTOR, KD_TP, DENDA_SPPT, JML_SPPT_YG_DIBAYAR, TGL_PEMBAYARAN_SPPT, TGL_REKAM_BYR_SPPT, NIP_REKAM_BYR_SPPT)
                                                        VALUES ( substr('$nop',1,2),substr('$nop',3,2),substr('$nop',5,3),substr('$nop',8,3),substr('$nop',11,3),substr('$nop',14,4),substr('$nop',18,1), '$tahun', '1','01','01' , '04', '$denda', '$jml_sppt_yg_dibayar', to_date('$datetime','yyyy-mm-dd hh24:mi:ss'), sysdate, '060000000000000000')");

                        if ($c) {
                            $aa .= "3. sukses<br>";
                        } else {
                            $aa .= "3. gagal <br>";
                        }
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            $data = array(
                                "Status" =>  array(
                                    "IsError" => "True",
                                    "ResponseCode" => '99',
                                    "ErrorDesc" => "System Error ->" . $aa
                                )
                            );
                            header('Content-Type: application/json');
                            // echo json_encode($data);
                            $konten = json_encode($data);
                            $this->db->set('IP_ADDRESS', $this->input->ip_address());
                            $this->db->set('KONTEN', $konten);
                            $this->db->set('KODE_BANK', $merchant);
                            $this->db->set('TANGGAL', 'sysdate', false);
                            $this->db->insert('LOG_SERVICE');
                            echo $konten;
                        } else {
                            $dtnop = $this->dataNop($nop);
                            $kec   = $this->getKecamatan($nop);
                            $kel   = $this->getKelurahan($nop);

                            $data = array(
                                "Nop"            =>  $q->KD_PROPINSI . $q->KD_DATI2 . $q->KD_KECAMATAN . $q->KD_KELURAHAN . $q->KD_BLOK . $q->NO_URUT . $q->KD_JNS_OP,
                                "Nama"           =>  $q->NM_WP_SPPT,
                                "Alamatobjek"    =>  $dtnop->JALAN_OP . ' ' . $dtnop->BLOK_KAV_NO_OP . ' RT ' . $dtnop->RT_OP . ' / RW ' . $dtnop->RW_OP,
                                "kelurahanobjek" => $kel->NM_KELURAHAN,
                                "kecamatanobjek" => $kec->NM_KECAMATAN,
                                "Kabupaten"      => "MALANG",
                                "TahunPajak"     => $q->THN_PAJAK_SPPT,
                                "JatuhTempo"     => $q->TGL_JATUH_TEMPO_SPPT,
                                "Pokok"          =>  (int) $q->PBB_YG_HARUS_DIBAYAR_SPPT,
                                "Denda"          => (int) $q->DENDA,
                                "Total"          =>  (int) $q->PBB_YG_HARUS_DIBAYAR_SPPT + (int) $q->DENDA,
                                "Pengesahan"     =>  $kdp,
                                "Status"         =>  array(
                                    "IsError"      => "False",
                                    "ResponseCode" => '11',
                                    "ErrorDesc"    => "Payment Sukses"
                                )
                            );
                            header('Content-Type: application/json');
                            // echo json_encode($data);
                            $konten = json_encode($data);
                            $this->db->set('IP_ADDRESS', $this->input->ip_address());
                            $this->db->set('KONTEN', $konten);
                            $this->db->set('KODE_BANK', $merchant);
                            $this->db->set('TANGGAL', 'sysdate', false);
                            $this->db->insert('LOG_SERVICE');
                            echo $konten;
                        }
                    }
                }
            } else {
                // data tidak di temukan
                $data = array();
                $data = array(

                    "Status"        =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
                header('Content-Type: application/json');
                // echo json_encode($data);
                $konten = json_encode($data);
                $this->db->set('IP_ADDRESS', $this->input->ip_address());
                $this->db->set('KONTEN', $konten);
                $this->db->set('KODE_BANK', $merchant);
                $this->db->set('TANGGAL', 'sysdate', false);
                $this->db->insert('LOG_SERVICE');
                echo $konten;
            } // end $q->kd_propinsi

        } //end post
    } //end payment



    public function reversal()
    {
        // echo $_SERVER['REQUEST_METHOD'];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $content  = file_get_contents("php://input");
            $decoded  = json_decode($content, true);

            $des = '';
            if (!isset($decoded['Nop'])) {
                $des .= 'parameter nop harus di sertakan,';
            } else {
                if (strlen($decoded['Nop']) != 18) {
                    $des .= 'nop harus 18 digit,';
                }
            }

            if (!isset($decoded['Tahun'])) {
                $des .= 'parameter tahun pajak harus di sertakan,';
            }

            if (!isset($decoded['Pokok'])) {
                $des .= 'parameter pokok harus di sertakan,';
            }

            if (!isset($decoded['Total'])) {
                $des .= 'parameter total harus di sertakan,';
            }

            // echo $decoded['kode_bank'];

            if (!isset($decoded['kode_bank'])) {
                $des .= 'parameter kode bank harus di sertakan';
            }

            if ($des != '') {
                $data = array(

                    "Status" =>  array(
                        "IsError"      => "True",
                        "ResponseCode" => '99',
                        "ErrorDesc"    => "System Error. " . substr($des, 0, -1)
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
                die();
            }

            $nop      = $decoded['Nop'];
            $tahun    = $decoded['Tahun'];
            $pokok    = $decoded['Pokok'];
            $denda    = $decoded['Denda'];
            $total    = $decoded['Total'];
            $merchant = $decoded['kode_bank'];

            // $cb = $this->db->query("select * from ref_bank@TOLAYANAN where kode_bank=$merchant")->row();
            $cb = $this->db->query("SELECT * FROM LAYANAN.REF_BANK where kode_bank=$merchant and status=1")->row();
            if (empty($cb)) {
                $data = array();
                $data = array(
                    "Status"     =>  array(
                        "IsError"      => "True",
                        "ResponseCode" => '91',
                        "ErrorDesc"    => "Kode bank tidak terdaftar"
                    )
                );
                header('Content-Type: application/json');
                // echo json_encode($data);
                $konten = json_encode($data);
                $this->db->set('IP_ADDRESS', $this->input->ip_address());
                $this->db->set('KONTEN', $konten);
                $this->db->set('KODE_BANK', $merchant);
                $this->db->set('TANGGAL', 'sysdate', false);
                $this->db->insert('LOG_SERVICE');
                echo $konten;
                die();
            } //end bank    

            $q = $this->ceksppt($nop, $tahun);
            if (empty($q)) {
                // data tidak ditemukan
                $data = array(
                    "Status"     =>  array(
                        "IsError"       => "True",
                        "ResponseCode"  => '34',
                        "ErrorDesc"     => "Data reversal tidak ditemukan"
                    )
                );
                header('Content-Type: application/json');
                // echo json_encode($data);
                $konten = json_encode($data);
                $this->db->set('IP_ADDRESS', $this->input->ip_address());
                $this->db->set('KONTEN', $konten);
                $this->db->set('KODE_BANK', $merchant);
                $this->db->set('TANGGAL', 'sysdate', false);
                $this->db->insert('LOG_SERVICE');
                echo $konten;
            } else {
                if ((int) $q->PBB_YG_HARUS_DIBAYAR_SPPT + (int) $q->DENDA != $total) {
                    $data = array(
                        "Status"     =>  array(
                            "IsError"       => "True",
                            "ResponseCode"  => '14',
                            "ErrorDesc"     => "Jumlah tagihan yang dibayarkan tidak sesuai"
                        )
                    );
                    header('Content-Type: application/json');
                    // echo json_encode($data);
                    $konten = json_encode($data);
                    $this->db->set('IP_ADDRESS', $this->input->ip_address());
                    $this->db->set('KONTEN', $konten);
                    $this->db->set('KODE_BANK', $merchant);
                    $this->db->set('TANGGAL', 'sysdate', false);
                    $this->db->insert('LOG_SERVICE');
                    echo $konten;
                } else {
                    $this->db->trans_start();

                    /* $this->db->query("INSERT INTO REVERSAL_PEMBAYARAN_SPPT@TOSPO14   
                                                 SELECT A.*,'009' kd_bank,SYSDATE tanggal FROM PEMBAYARAN_SPPT@tospo14 A
                                                 WHERE KD_PROPINSI =SUBSTR('$nop',1,2) 
                                                 AND KD_DATI2=SUBSTR('$nop',3,2)
                                                 AND KD_KECAMATAN=SUBSTR('$nop',5,3)
                                                 AND KD_KELURAHAN=SUBSTR('$nop',8,3)
                                                 AND KD_BLOK=SUBSTR('$nop',11,3) 
                                                 AND NO_URUT=SUBSTR('$nop',14,4) 
                                                 AND KD_JNS_OP=SUBSTR('$nop',18,1)
                                                 AND THN_PAJAK_SPPT='$tahun'"); */
                    $this->db->query("INSERT INTO REVERSAL_PEMBAYARAN_SPPT
                                                 SELECT A.*,'$merchant' kd_bank,SYSDATE tanggal FROM PEMBAYARAN_SPPT A
                                                 WHERE KD_PROPINSI =SUBSTR('$nop',1,2) 
                                                 AND KD_DATI2=SUBSTR('$nop',3,2)
                                                 AND KD_KECAMATAN=SUBSTR('$nop',5,3)
                                                 AND KD_KELURAHAN=SUBSTR('$nop',8,3)
                                                 AND KD_BLOK=SUBSTR('$nop',11,3) 
                                                 AND NO_URUT=SUBSTR('$nop',14,4) 
                                                 AND KD_JNS_OP=SUBSTR('$nop',18,1)
                                                 AND THN_PAJAK_SPPT='$tahun'");


                    /* $this->db->query("DELETE  FROM PEMBAYARAN_SPPT@TOSPO14 
                                                     where kd_propinsi  =substr('$nop',1,2) 
                                                     and kd_dati2       =substr('$nop',3,2)
                                                     and kd_kecamatan   =substr('$nop',5,3)
                                                     and kd_kelurahan   =substr('$nop',8,3)
                                                     and kd_blok        =substr('$nop',11,3) 
                                                     and no_urut        =substr('$nop',14,4) 
                                                     and kd_jns_op      =substr('$nop',18,1)
                                                     and thn_pajak_sppt =$tahun"); */
                    $this->db->query("DELETE  FROM PEMBAYARAN_SPPT
                                                     where kd_propinsi  =substr('$nop',1,2) 
                                                     and kd_dati2       =substr('$nop',3,2)
                                                     and kd_kecamatan   =substr('$nop',5,3)
                                                     and kd_kelurahan   =substr('$nop',8,3)
                                                     and kd_blok        =substr('$nop',11,3) 
                                                     and no_urut        =substr('$nop',14,4) 
                                                     and kd_jns_op      =substr('$nop',18,1)
                                                     and thn_pajak_sppt =$tahun");

                    $this->db->query("UPDATE  sppt@to17 set status_pembayaran_sppt=0 where thn_pajak_sppt='$tahun'and kd_propinsi =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1)");

                    $this->db->query("DELETE  from PEMBAYARAN_SPPT@to17 
                                                     where kd_propinsi  =substr('$nop',1,2) 
                                                     and kd_dati2       =substr('$nop',3,2)
                                                     and kd_kecamatan   =substr('$nop',5,3)
                                                     and kd_kelurahan   =substr('$nop',8,3)
                                                     and kd_blok        =substr('$nop',11,3) 
                                                     and no_urut        =substr('$nop',14,4) 
                                                     and kd_jns_op      =substr('$nop',18,1)
                                                     and thn_pajak_sppt =$tahun");

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE) {
                        $data = array(
                            "Status" =>  array(
                                "IsError"      => "True",
                                "ResponseCode" => '99',
                                "ErrorDesc"    => "System Error"
                            )
                        );
                        header('Content-Type: application/json');
                        // echo json_encode($data);
                        $konten = json_encode($data);
                        $this->db->set('IP_ADDRESS', $this->input->ip_address());
                        $this->db->set('KONTEN', $konten);
                        $this->db->set('KODE_BANK', $merchant);
                        $this->db->set('TANGGAL', 'sysdate', false);
                        $this->db->insert('LOG_SERVICE');
                        echo $konten;
                    } else {
                        $dtnop = $this->dataNop($nop);
                        $kec   = $this->getKecamatan($nop);
                        $kel   = $this->getKelurahan($nop);

                        $data = array(
                            "Nop"        =>  $q->KD_PROPINSI . $q->KD_DATI2 . $q->KD_KECAMATAN . $q->KD_KELURAHAN . $q->KD_BLOK . $q->NO_URUT . $q->KD_JNS_OP,
                            "Nama"       =>  $q->NM_WP_SPPT,
                            "Alamatobjek"     =>  $dtnop->JALAN_OP . ' ' . $dtnop->BLOK_KAV_NO_OP . ' RT ' . $dtnop->RT_OP . ' / RW ' . $dtnop->RW_OP,
                            "kelurahanobjek" => $kel->NM_KELURAHAN,
                            "kecamatanobjek" => $kec->NM_KECAMATAN,
                            "Kabupaten" => "MALANG",
                            "TahunPajak" => $q->THN_PAJAK_SPPT,
                            "JatuhTempo" => $q->TGL_JATUH_TEMPO_SPPT,
                            "Pokok"      =>  (int) $q->PBB_YG_HARUS_DIBAYAR_SPPT,
                            "Denda"      => (int) $q->DENDA,
                            "Total"      =>  (int) $q->PBB_YG_HARUS_DIBAYAR_SPPT + (int) $q->DENDA,
                            "Status"     =>  array(
                                "IsError"      => "False",
                                "ResponseCode" => '22',
                                "ErrorDesc"    => "Reversal sukses"
                            )
                        );
                        header('Content-Type: application/json');
                        // echo json_encode($data);
                        $konten = json_encode($data);
                        $this->db->set('IP_ADDRESS', $this->input->ip_address());
                        $this->db->set('KONTEN', $konten);
                        $this->db->set('KODE_BANK', $merchant);
                        $this->db->set('TANGGAL', 'sysdate', false);
                        $this->db->insert('LOG_SERVICE');
                        echo $konten;
                    }
                }
            }
        }
    }



    function payment_test()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $content  = file_get_contents("php://input");
            $decoded  = json_decode($content, true);

            $des = '';
            if (empty($decoded['Nop'])) {
                $des .= 'parameter nop harus di sertakan,';
            } else {
                if (strlen($decoded['Nop']) != 18) {
                    $des .= 'panjang nop harus 18 digit,';
                }
            }

            if (empty($decoded['kode_bank'])) {
                $des .= 'parameter kode bank harus di sertakan,';
            }

            if (empty($decoded['DateTime'])) {
                $des .= 'parameter date time harus di sertakan,';
            }

            if (empty($decoded['Tahun'])) {
                $des .= 'parameter tahun pajak harus di sertakan,';
            }

            if (empty($decoded['Pokok'])) {

                $des .= 'parameter pokok harus di sertakan,';
            }

            if (empty($decoded['Total'])) {
                $des .= 'parameter total harus di sertakan,';
            }

            if ($des != '') {
                $data = array(

                    "Status" =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '99',
                        "ErrorDesc" => "System Error. " . substr($des, 0, -1)
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
                die();
            }

            $nop      = $decoded['Nop'];
            $merchant = $decoded['kode_bank'];
            $datetime = $decoded['DateTime'];
            $tahun    = $decoded['Tahun'];
            $pokok    = $decoded['Pokok'];
            $denda    = $decoded['Denda'];
            $total    = $decoded['Total'];



            // $cb = $this->db->query("select * from ref_bank@TOLAYANAN where kode_bank=$merchant")->row();
            $cb = $this->db->query("SELECT * FROM LAYANAN.REF_BANK where kode_bank=$merchant and status=1")->row();
            if (empty($cb)) {
                $data = array();
                $data = array(
                    "Status"     =>  array(
                        "IsError"      => "True",
                        "ResponseCode" => '91',
                        "ErrorDesc"    => "Kode bank tidak terdaftar"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
                die();
            } //end bank    

            $q = $this->ceksppt($nop, $tahun);
            if (!empty($q->KD_PROPINSI)) {
                $cb = $this->cekBayar($nop, $tahun);
                /*if ($cb > 0) {
                    // tagihan telah lunas
                    $data = array();
                    $data = array(
                        "Status"        =>  array(
                            "IsError" => "True",
                            "ResponseCode" => '13',
                            "ErrorDesc" => "Data Tagihan Telah Lunas"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {*/
                // belum lunas
                if (((int) $q->PBB_YG_HARUS_DIBAYAR_SPPT + (int) $q->DENDA) != $total) {
                    $data = array();
                    $data = array(
                        "Status"     =>  array(
                            "IsError"      => "True",
                            "ResponseCode" => '14',
                            "ErrorDesc"    => "Jumlah tagihan yang dibayarkan tidak sesuai"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                } else {
                    // sukses


                    $dtnop = $this->dataNop($nop);
                    $kec   = $this->getKecamatan($nop);
                    $kel   = $this->getKelurahan($nop);

                    $data = array(
                        "Nop"            =>  $q->KD_PROPINSI . $q->KD_DATI2 . $q->KD_KECAMATAN . $q->KD_KELURAHAN . $q->KD_BLOK . $q->NO_URUT . $q->KD_JNS_OP,
                        "Nama"           =>  $q->NM_WP_SPPT,
                        "Alamatobjek"    =>  $dtnop->JALAN_OP . ' ' . $dtnop->BLOK_KAV_NO_OP . ' RT ' . $dtnop->RT_OP . ' / RW ' . $dtnop->RW_OP,
                        "kelurahanobjek" => $kel->NM_KELURAHAN,
                        "kecamatanobjek" => $kec->NM_KECAMATAN,
                        "Kabupaten"      => "MALANG",
                        "TahunPajak"     => $q->THN_PAJAK_SPPT,
                        "JatuhTempo"     => $q->TGL_JATUH_TEMPO_SPPT,
                        "Pokok"          =>  (int) $q->PBB_YG_HARUS_DIBAYAR_SPPT,
                        "Denda"          => (int) $q->DENDA,
                        "Total"          =>  (int) $q->PBB_YG_HARUS_DIBAYAR_SPPT + (int) $q->DENDA,
                        "Pengesahan"     =>  $kdp,
                        "Status"         =>  array(
                            "IsError"      => "False",
                            "ResponseCode" => '11',
                            "ErrorDesc"    => "Payment Sukses"
                        )
                    );
                    header('Content-Type: application/json');
                    echo json_encode($data);
                }
                // }
            } else {
                // data tidak di temukan
                $data = array();
                $data = array(

                    "Status"        =>  array(
                        "IsError" => "True",
                        "ResponseCode" => '10',
                        "ErrorDesc" => "Data Tagihan Tidak Ditemukan"
                    )
                );
                header('Content-Type: application/json');
                echo json_encode($data);
            } // end $q->kd_propinsi

        } //end post
    }
}

<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
class Izin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*$this->load->model('Mpokok');
        $this->load->library('datatables');*/
        /*   if (ENVIRONMENT == 'development') {
            $pass = '1a1dc91c907325c69271ddf0c944bc72';
        } else {
            $pass = '1a1dc91c907325c69271ddf0c944bc72';
        } */

        $pass = md5("Perizinan2020");
        if (!(isset($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])
            && $_SERVER['PHP_AUTH_USER'] == 'bapenda'
            && md5($_SERVER['PHP_AUTH_PW']) == $pass)) {
            header('WWW-Authenticate: Basic realm="Restricted area"');
            header('HTTP/1.1 401 Unauthorized');
            exit;
        }
    }

    private function ceksppt($nop, $tahun_pajak)
    {

        $res = $this->db->query("SELECT * from sppt@to17
        where  thn_pajak_sppt='$tahun_pajak'and kd_propinsi =substr('$nop',1,2) and kd_dati2=substr('$nop',3,2) and kd_kecamatan=substr('$nop',5,3) and kd_kelurahan=substr('$nop',8,3) and kd_blok=substr('$nop',11,3) and no_urut=substr('$nop',14,4) and kd_jns_op=substr('$nop',18,1) ")->row();
        return $res;
    }

    private function dataNop($nop)
    {
        return $this->db->query("SELECT * FROM DAT_OBJEK_PAJAK@TO17
                                where kd_propinsi =substr('$nop',1,2) 
                                 and kd_dati2=substr('$nop',3,2)
                                 and kd_kecamatan=substr('$nop',5,3)
                                 and kd_kelurahan=substr('$nop',8,3)
                                and kd_blok=substr('$nop',11,3) 
                                and no_urut=substr('$nop',14,4) 
                                and kd_jns_op=substr('$nop',18,1)")->row();
    }

    private function getKecamatan($nop)
    {
        return $this->db->query("SELECT * FROM REF_KECAMATAN@TO17
                                    where kd_kecamatan =substr('$nop',5,3)")->row();
    }

    private function getKelurahan($nop)
    {
        return $this->db->query("SELECT * FROM REF_KELURAHAN@TO17
                               where kd_kecamatan=substr('$nop',5,3)
                                 and kd_kelurahan=substr('$nop',8,3)")->row();
    }

    function index()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $nop         = $this->input->get('nop');

            $des = '';
            $rc = '';
            $ie = false;
            if (empty($nop)) {
                $des .= 'parameter nop harus di sertakan,';
            }

            if (strlen($nop) != 18) {
                $des .= 'panjang nop harus 18 digit,';
            }
            // cek sppt



            if ($des <> '') {
                $data = [
                    "ErrorDesc" => $des
                ];
            } else {
                $data = [];
                for ($i = date('Y'); $i >= date('Y') - 1; $i--) {
                    // $des .= $i . ',';
                    $query = $this->ceksppt($nop, $i);
                    $dtnop = $this->dataNop($nop);
                    $kec   = $this->getKecamatan($nop);
                    $kel   = $this->getKelurahan($nop);

                    if ($query) {

                        /* 
                                "NOP": "351509000200302060",
        "NAMA_WP": "KARSUDI",
		"ALAMAT_WP": "KO TAMAN ANGGUN SJT B3-5 RT 000 RW 00 POPOH WONOAYU",
        "THN_PAJAK": "2021",
        "TAGIHAN_PAJAK": "123614",
        "STATUS_PEMBAYARAN": "1",
        "ALAMAT_OP": "KO TAMAN ANGGUN SJT B3-5",
        "RT_OP": "000",
        "RW_OP": "00",
		"KELURAHAN_OP": "POPOH",
		"KECAMATAN_OP": "WONOAYU",
        "LUAS_BUMI": "90",
        "LUAS_BNG": "36",
        "NJOP_BUMI": "92880000",
        "NJOP_BNG": "34848000",
        "LAT": "-7.437729",
		"LON": "112.622304"

                        */

                        $data[] = [
                            "NOP"            => $query->KD_PROPINSI . $query->KD_DATI2 . $query->KD_KECAMATAN . $query->KD_KELURAHAN . $query->KD_BLOK . $query->NO_URUT . $query->KD_JNS_OP,
                            "NAMA"           =>  $query->NM_WP_SPPT,
                            "ALAMATA_WP" => $query->JLN_WP_SPPT . ' ' . $query->BLOK_KAV_NO_WP_SPPT . ' RT ' . $query->RT_WP_SPPT . ' RW ' . $query->RW_WP_SPPT . ' ' . $query->KELURAHAN_WP_SPPT,
                            "THN_PAJAK" => $query->THN_PAJAK_SPPT,
                            "TAGIHAN_PAJAK" => $query->PBB_YG_HARUS_DIBAYAR_SPPT,
                            "STATUS_PEMBAYARAN"=> $query->STATUS_PEMBAYARAN_SPPT,
                            "ALAMAT_OP"=> $dtnop->JALAN_OP . ' ' . $dtnop->BLOK_KAV_NO_OP ,
                            "RT_OP" => $dtnop->RT_OP,
                            "RW_OP" => $dtnop->RW_OP,
                            "KELURAHAN_OP" => $kel->NM_KELURAHAN,
                            "KECAMATAN_OP" => $kec->NM_KECAMATAN,
                            "LUAS_BUMI" => $query->LUAS_BUMI_SPPT,
                            "LUAS_BNG" => $query->LUAS_BNG_SPPT,
                            "NJOP_BUMI" => $query->NJOP_BUMI_SPPT,
                            "NJOP_BNG" => $query->NJOP_BNG_SPPT,
                            "LAT" => '',
                            "LONG" => ''

                        ];
                    }
                }
            }




            header('Content-Type: application/json');
            // echo json_encode($data);
            $konten = json_encode($data);
            echo $konten;
        }
    }
}
